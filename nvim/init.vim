
call plug#begin('~/.local/share/nvim/plugged')

Plug 'lervag/vimtex'
Plug 'bling/vim-airline'
Plug 'jalvesaq/Nvim-R', {'branch': 'stable'}
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()

set number
set relativenumber

" Setting for vimtex
let g:tex_flavor = 'latex'

" settings for zathura
let g:vimtex_view_general_viewer = 'zathura'

" settings for nvim-R
let R_assign_map = ''

" keymaps
map <M-p> i<p></p><Esc>3hi
map <M-i> i<i></i><Esc>3hi
map <M-s> i<strong></strong><Esc>8hi
map <M-h> i<h2></h2><Esc>4hi
map <M-f> i\begin{}<Enter>\end{}<Esc>k2li
map <M-t> i\textit{}<Esc>i
map <M-d> i\textbf{}<Esc>i
map <M-v> i\vspace{0.3cm}<Esc>
map <M-V> i\vspace{0.5cm}<Esc>

nnoremap tn :tabnew<Space>
nnoremap tj :tabnext<CR>
nnoremap tk :tabprev<CR>
nnoremap th :tabfirst<CR>
nnoremap tl :tablast<CR>

" Sourcing the coc config
source $HOME/.config/nvim/plug-config/coc.vim
